"""
March Madness 2016, Kaggle
@author: Gianluca Rossi
@title: FeaturizerFlow
"""

import os
import csv
import sys
import re
import pandas as pd

_SEASONS_TO_PREDICT = {2012, 2013, 2014, 2015}
_FILE_PATH = '../data/'
_TOURNAMENT_FILENAME = 'TourneyDetailedResults.csv'
_SEEDS_FILENAME = 'TourneySeeds.csv'


def get_training_and_test_sets():
    """Prepare training set for TrainingFlow and PredictingFlow.

    Note: The dataset will be inclusive of all observations in *TourneyDetailedResults*. To avoid information leaking
     both at training and predicting time we will remove from the results of the season we are trying to predict on a
     case by case basis.

    :return: a pandas DataFrame containing the training set and a list with the outcomes for each game played
    """
    # load tournament results
    results = []
    with open(os.path.join(_FILE_PATH, _TOURNAMENT_FILENAME), encoding='utf-8') as csvfile:
        tournament_rows = csv.reader(csvfile, delimiter=',')
        csv_headings = next(tournament_rows)
        for row in tournament_rows:
            results.append([item for item in row])
    print('[X] Loaded data for {} tournament games from `{}`'.format(len(results), _TOURNAMENT_FILENAME))

    # create list of observations for winning team
    idxs_win_team = [csv_headings.index(item) for item in csv_headings if all([item[0] == "W", item != "Wloc"])]
    idxs_lose_team = [csv_headings.index(item) for item in csv_headings if item[0] == "L"]
    seasons = [item[0] for item in results]
    win_team_loc = [item[6] for item in results]

    win_team_data = []
    for i, item in enumerate(results):
        win_team_data.append([item[n] for n in idxs_win_team])
    lose_team_data = []
    for i, item in enumerate(results):
        lose_team_data.append([item[n] for n in idxs_lose_team])

    # create the index for our training file using the format 'season_team1_team2'
    win_teams = [team[0] for team in win_team_data]
    lose_teams = [team[0] for team in lose_team_data]
    idx_train = ['_'.join([season, min(team1, team2), max(team1, team2)])
                 for season, team1, team2 in zip(seasons, win_teams, lose_teams)]

    # extract seeds and regions
    seeds = []
    with open(os.path.join(_FILE_PATH, _SEEDS_FILENAME), encoding='utf-8') as csvfile:
        seeds_rows = csv.reader(csvfile, delimiter=',')
        seeds_headings = next(seeds_rows)
        for row in seeds_rows:
            seeds.append([item for item in row])
    seeds_and_regions = [[item[0], item[2], re.findall(r'[a-zA-Z]', item[1])[0], int(re.findall(r'\d+', item[1])[0])]
                 for item in seeds]

    # transform the categorical variable `region` into dummy variables
    seeds_and_dummy_regions = [item for item in [[x[0], x[1], get_dummies(x[2]), x[3]] for x in seeds_and_regions]]
    seeds_and_dummy_regions_flattened = []
    for list_of_lists in seeds_and_dummy_regions:
        row = []
        for item in list_of_lists:
            if isinstance(item, list):
                for i in item:
                    row.append(i)
            else:
                row.append(item)
        seeds_and_dummy_regions_flattened.append(row)

    # create a nested dictionary comprehension where the outer key is the team and the inner key is the year
    seeds_and_region_dict = {t[1]: {y[0]: [y[2], y[3], y[4], y[5]]
                                    for y in seeds_and_dummy_regions_flattened if y[1] == t[1]}
                             for t in seeds_and_dummy_regions_flattened}

    # check that `seeds_and_region_dict` includes all matches comparing its length to the number of raw observations
    if __debug__:
        if not len(seeds) == sum(sum(1 for y in seeds_and_region_dict[t].keys()) for t in seeds_and_region_dict.keys()):
            raise AssertionError('The number of observations between raw file and `seeds_and_region_dict` do not match')

    # create a list of lists with region and seed for each team playing a game; use the dictionary we just created to
    # look up the region and seed for each team
    game_seeds_and_regions = [[seeds_and_region_dict[min(team1, team2)][season][0],
                               seeds_and_region_dict[min(team1, team2)][season][1],
                               seeds_and_region_dict[max(team1, team2)][season][0],
                               seeds_and_region_dict[max(team1, team2)][season][1]]
                              for season, team1, team2 in zip(seasons, win_teams, lose_teams)]

    # join winning and losing team data; teams with lower id will be attached first as per submission file
    # specification; also create a list of outcomes for each game where 0 represent a lost for the team with lowest
    # team id and 1 a win
    game_statistics = []
    game_outcomes = []
    for zipped in zip(win_team_data, lose_team_data):
        if zipped[0][0] < zipped[1][0]:
            zipped[0].extend(zipped[1])
            game_statistics.append(zipped[0])
            game_outcomes.extend([1])
        else:
            zipped[1].extend(zipped[0])
            game_statistics.append(zipped[1])
            game_outcomes.extend([0])

    # create a new list of lists which includes the season, game stats, seeds and regions
    seasons = [[int(s)] for s in seasons]
    game_features = [season + game_stats + game_seeds
                     for season, game_stats, game_seeds in zip(seasons, game_statistics, game_seeds_and_regions)]

    print('[X] Generate tournament training dataset')
    print('[X] Generated list of tournament outcomes')
    return pd.DataFrame(game_features, index=idx_train), pd.DataFrame(game_outcomes, index=idx_train)


def get_dummies(x):
    """Convert the region variable from a categorical representation to a dummy variable

    :param x: the category we want to transform
    :return: a list representing a binary representation (dummy variables)"""
    if not isinstance(x, str):
        raise TypeError('Input should be a string')
    if x not in ['Y', 'W', 'Z', 'X']:
        raise OutOfRangeError('Category should be one of the four predefined (Y, W, Z, X)')

    if x == 'Y':
        return [1, 0, 0]
    if x == 'W':
        return [0, 1, 0]
    if x == 'Z':
        return [0, 0, 1]
    else:
        return [0, 0, 0]


class OutOfRangeError(ValueError):
    pass


def main():
    """Visual check"""
    get_training_and_test_sets()


if __name__ == '__main__':
    sys.exit(main())
