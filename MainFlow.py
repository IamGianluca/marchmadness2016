"""
March Madness 2016, Kaggle
@author: Gianluca Rossi
@title: MainFlow
"""

import FeaturizerFlow
import TrainingFlow
import PredictingFlow


_SEASONS = {2012, 2013, 2014, 2015}


def start_flow():
    """
    For each season, use the data from that year regular season and old regular seasons and tournaments data to fit
     a statistical model. We will then use this model to predict the results of the seeded teams in that year
     tournament.
    """
    train, outcomes = FeaturizerFlow.get_training_and_test_sets()
    model = TrainingFlow.get_logistic_classifier(_SEASONS, train, outcomes)
    # PredictingFlow.getPredictions(model)


if __name__ == '__main__':
    start_flow()
