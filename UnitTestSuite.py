"""
March Madness 2016, Kaggle
@author: Gianluca Rossi
@title: Unit Test Suite
"""

import FeaturizerFlow
import PredictingFlow
import unittest
import numpy as np


class DummyTest(unittest.TestCase):
    known_values = (('X', [0, 0, 0]),
                    ('Y', [1, 0, 0]),
                    ('W', [0, 1, 0]),
                    ('Z', [0, 0, 1]))

    def test_get_dummies(self):
        """`get_dummies()` should give known result with known input"""
        for category, dummy in self.known_values:
            result = FeaturizerFlow.get_dummies(category)
            self.assertEqual(dummy, result)

    def test_wrong_type(self):
        """`get_dummies()` should return a TypeError if the input is not a string"""
        self.assertRaises(TypeError, FeaturizerFlow.get_dummies, 1)

    def test_unknown_category(self):
        """`get_dummies()` should return a *OutOfRangeError is the category is a string but not in the predefined
        range"""
        self.assertRaises(FeaturizerFlow.OutOfRangeError, FeaturizerFlow.get_dummies, 'A')

    def test_blank_category(self):
        """`get_dummies()` should return a *OutOfRangeError* if the input is an empty string"""
        self.assertRaises(FeaturizerFlow.OutOfRangeError, FeaturizerFlow.get_dummies, '')


class LogLossTest(unittest.TestCase):
    def test_logloss_score(self):
        """Check that `log_loss_score()` returns the correct results given the right input"""
        y = [1, 0, 0, 1]
        y_hat = [.9, .1, .2, .65]
        result = PredictingFlow.log_loss_score(y_hat=y_hat, y=y)
        from sklearn.metrics import log_loss
        self.assertEqual(log_loss(y, [[.1, .9], [.9, .1], [.8, .2], [.35, .65]]), result)

    def test_wrong_input(self):
        """Exception should be raised if input are in the wrong format"""
        self.assertRaises(TypeError, PredictingFlow.log_loss_score, y=[1, 0, 'E', 1], y_hat=[.9, .1, .2, .65])

    def test_nan_prediction(self):
        """Test should not run if at least one prediction is not in a valid format (float)"""
        self.assertRaises(ValueError, PredictingFlow.log_loss_score, y=[1, 0, 0, 1], y_hat=[.9, .1, np.NaN, .65])


if __name__ == '__main__':
    unittest.main()
