"""
March Madness 2016, Kaggle
@author: Gianluca Rossi
@title: PredictingFlow
"""

import pandas as pd
import csv
import datetime
import scipy as sp
import numpy as np


def getPredictions():
    """
    The format is a list of every possible matchup between the tournament teams. Since team1 vs. team2 is the same
     as team2 vs. team1, we only include the game pairs where team1 has the lower team id. For example, in a tournament
     of 68 teams (64 + 4 play-in teams), you will predict (68*67)/2 = 2278 matchups.

    Each game has a unique id created by concatenating the season in which the game was played, the team1 id, and the
     team2 id. For example, "2013_1104_1129" indicates team 1104 played team 1129 in the year 2013. You must predict
     the probability that the team with the lower id beats the team with the higher id.

    The resulting submission format looks like the following, where "pred" represents the predicted probability that
     the first team will win:

    # id,pred
    # 2012_1104_1124,0.5
    # 2012_1104_1125,0.5
    # 2012_1104_1140,0.5
    # ...

    :return:
    """

    """
    As a first submission we will simply use a probability of 0.5 for each match. We are simply interested on have
     a quick solution we can score, in order to understand the competition format. We will then work on improving this
     soft benchmark using more advanced techniques.
    """
    seeds_file = 'TourneySeeds.csv'
    path = '../data/'
    seeds_full_path = ''.join([path, seeds_file])
    seeds = pd.read_csv(seeds_full_path)

    result_path = '../predictions/'
    submission_file_name = getFormattedFileName(filename='seeds_submission.csv')
    submission_file_full_path = ''.join((result_path, submission_file_name))

    seasons = [2012, 2013, 2014, 2015]
    for season in seasons:
        # source teams for current season tournament
        current_season_seeds = seeds.loc[seeds.Season == season].reset_index(drop=True)
        teams = current_season_seeds.Team.unique()

        with open(submission_file_full_path, mode='a', encoding='utf-8') as predictions:
            csv_writer = csv.writer(predictions, delimiter=',')
            if season == 2012:
                csv_writer.writerow('id,pred'.split(','))
            for team1 in teams:
                for team2 in teams:
                    if team1 < team2:
                        # give .03 more probability to win for every 1 ranking position of difference
                        team1_rank = int(str(seeds.loc[(seeds.Season == season) & (seeds.Team == team1), 'Seed'].values)[3:5])
                        team2_rank = int(str(seeds.loc[(seeds.Season == season) & (seeds.Team == team2), 'Seed'].values)[3:5])
                        p = .5 + (team2_rank - team1_rank) * .03
                        csv_writer.writerow('{0}_{1}_{2},{3}'.format(season, team1, team2, p).split(','))
        print('[X] Saved predictions for {0} season'.format(season))


def getFormattedFileName(filename, fmt='%Y%m%d-%H%M%S_{fname}'):
    """
    Helper function to format the submission file in a way that can include the data and time of when the file was
     generated.

    :param filename: the original file name
    :param fmt: datatime format
    :return: a file name which is the result of concatenating the current datetime with the original file name
    """
    return datetime.datetime.now().strftime(fmt).format(fname=filename)


def log_loss_score(y_hat, y):
    """
    A smaller log loss is better. Games which are not played are ignored in the scoring. Play-in games are also
     ignored (only the games among the final 64 teams are scored). The use of the logarithm provides extreme punishments
     for being both confident and wrong. In the worst possible case, a prediction that something is true when it is
     actually false will add infinite to your error score. In order to prevent this, predictions are bounded away from
     the extremes by a small value.

    :param y_hat: predicted game results
    :param y: observed game results
    :return: LogLoss score
    """
    if not all([isinstance(n, float) for n in y_hat]):
        raise TypeError('At least one element of `y_hat` is not a `float`')
    if any([np.isnan(n) for n in y_hat]):
        raise ValueError('The list of predicted outcomes contains a NaN value')

    y = np.array(y)
    y_hat = np.array(y_hat)
    return (-1/len(y)) * np.sum(y * sp.log(y_hat) + (1 - y) * sp.log(1 - y_hat), axis=0)


if __name__ == '__main__':
    getPredictions(2012)
