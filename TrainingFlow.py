"""
March Madness 2016, Kaggle
@author: Gianluca Rossi
@title: TrainingFlow
"""

import sys
from sklearn.linear_model import LogisticRegression
import numpy as np


def get_logistic_classifier(seasons, train, outcomes):
    """Train logistic regression model

     Note that when the data are not independent cross-validation becomes more difficult as leaving out an observation
     does not remove all the associated information due to the correlations with other observations. For time series
     forecasting, a cross-validation statistic is obtained as follows:

     1. Fit the model to the data y1,…,yt and let y^t+1 denote the forecast of the next observation. Then compute the
        error (et+1∗=yt+1−y^t+1) for the forecast observation.
     2. Repeat step 1 for t=m,…,n−1 where m is the minimum number of observations needed for fitting the model.
     3. Compute the MSE from em+1∗,…,en∗.

    (Source: Rob J Hyndman, http://robjhyndman.com/hyndsight/crossvalidation/)

    :param seasons: a list of tournament seasons we want to predict
    :param train: a pandas DataFrame containing the training dataset
    :param outcomes: a list of tournament's game results
    :return: a list of trained logistic classifiers
    """
    classifiers = {}
    accuracy_test_set = []

    for season in seasons:
        # define training and test sets
        x_train = train.loc[train[0] < season, :]
        x_test = train.loc[train[0] == season, :]

        y_train = outcomes[:x_train.shape[0]]
        y_test = outcomes[x_train.shape[0]:x_train.shape[0] + x_test.shape[0]]

        # fit logistic classifier
        classifier = LogisticRegression(penalty='l2', n_jobs=-1)
        classifier.fit(x_train, np.ravel(y_train))

        # compute accuracy on test set
        accuracy_test_set.append(classifier.score(x_test, y_test))
        classifiers[season] = classifier

    print('[X] Logistic classifier training completed')
    print('\tAccuracy on test set: {0:.4f}'.format(np.mean(accuracy_test_set, axis=0)))
    return classifiers


def main():
    # TODO: create a synthetic dataset I can use to test *get_logistic_classifier()*
    """Check that the training pipeline is doing what expected"""
    # get_logistic_classifier(seasons={2012},
    #                         train=pd.DataFrame([[2012, 0, 0, 1], [2013, 0, 0, 1]],
    #                                            index=['2012_1181_1191', '2013_1845_1981']),
    #                         outcomes=[1, 0])


if __name__ == '__main__':
    sys.exit(main())
